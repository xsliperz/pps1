#include <iostream>
#include <memory>
#include <string>

// Product
class Building
{
private:
	std::string stena;
	std::string pol;
	std::string krisha;

public:
	Building() { }
	~Building() { }

	void SetStena(const std::string& d) { stena = d; }
	void SetPol(const std::string& s) { pol = s; }
	void SetKrisha(const std::string& t) { krisha = t; }

	void ShowBuilding()
	{
		std::cout << " Building " << std::endl
			<< "Stena is " << stena
			<< ", Pol is " << pol
			<< " and Krisha is " << krisha
			<< " . " << std::endl;
	}
};


// Abstract Builder
class DomBuilder
{
protected:
	std::shared_ptr<Building> dom;
public:
	DomBuilder() {}
	virtual ~DomBuilder() {}
	std::shared_ptr<Building> GetBuilding() { return dom; }

	void createNewBuildingProduct() { dom.reset(new Building); }

	virtual void buildStena() = 0;
	virtual void buildPol() = 0;
	virtual void buildKrisha() = 0;

};


// ConcreteBuilder
class KerpichnDomBuilder : public DomBuilder
{
public:
	KerpichnDomBuilder() : DomBuilder() {}
	~KerpichnDomBuilder() {}

	void buildStena() { dom->SetStena("kerpichn"); }
	void buildPol() { dom->SetPol("beton"); }
	void buildKrisha() { dom->SetKrisha("shifer"); }
};

// ConcreteBuilder
class DerewyanDomBuilder : public DomBuilder
{
public:
	DerewyanDomBuilder() : DomBuilder() {}
	~DerewyanDomBuilder() {}

	void buildStena() { dom->SetStena("brewna"); }
	void buildPol() { dom->SetPol("iz dosok"); }
	void buildKrisha() { dom->SetKrisha("cherepiza"); }

};


// Director
class Director
{
private:
	DomBuilder* houseBuilder;
public:
	Director() : houseBuilder(NULL) {}
	~Director() { }

	void SetHouseBuilder(DomBuilder* b) { houseBuilder = b; }
	std::shared_ptr<Building> GetBuilding() { return houseBuilder->GetBuilding(); }
	void ConstructHouse()
	{
		houseBuilder->createNewBuildingProduct();
		houseBuilder->buildStena();
		houseBuilder->buildPol();
		houseBuilder->buildKrisha();
	}
};


int main()
{
	Director direktor;

	KerpichnDomBuilder kerpichnoDomBuilder;
	direktor.SetHouseBuilder(&kerpichnoDomBuilder);
	direktor.ConstructHouse();
	std::shared_ptr<Building> dom = direktor.GetBuilding();
	dom->ShowBuilding();

	DerewyanDomBuilder derewyanoDomBuilder;
	direktor.SetHouseBuilder(&derewyanoDomBuilder);
	direktor.ConstructHouse();
	dom = direktor.GetBuilding();
	dom->ShowBuilding();

	system("PAUSE");

	return EXIT_SUCCESS;
}